const sketch = require('sketch');
const Settings = require('sketch/settings');
const BrowserWindow = require('sketch-module-web-view');
const FormData = require('sketch-polyfill-fetch/lib/form-data');
const URL = require('url-parse');

/**
 * START: self-managed GitLab instance configuration.
 * Edit these values for your instance. Defaults to gitlab.com configuration.
 */
const GITLAB_INSTANCE_PROTOCOL = 'https';
const GITLAB_INSTANCE_HOST = 'gitlab.com';
const GITLAB_OAUTH_APPLICATION_ID =
  'eb2369ae7c96c1c2f9c1fb69a4461185518e127c779040636c73fffc29e061d7';
const GITLAB_OAUTH_REDIRECT_URI = `${GITLAB_INSTANCE_PROTOCOL}://${GITLAB_INSTANCE_HOST}`;
/**
 * END: self-managed GitLab instance configuration.
 */

const GITLAB_INSTANCE_BASE_URL = `${GITLAB_INSTANCE_PROTOCOL}://${GITLAB_INSTANCE_HOST}`;
const GITLAB_API_BASE_URL = `${GITLAB_INSTANCE_BASE_URL}/api/v4`;
const GITLAB_GRAPHQL_BASE_URL = `${GITLAB_INSTANCE_BASE_URL}/api/graphql`;
const GITLAB_OAUTH_AUTH_URL = `${GITLAB_INSTANCE_BASE_URL}/oauth/authorize?client_id=${GITLAB_OAUTH_APPLICATION_ID}&redirect_uri=${GITLAB_OAUTH_REDIRECT_URI}&response_type=token&state=test&scope=api+read_user+profile`;

const resetPluginState = () => {
  Settings.setSettingForKey('access-token', undefined);
  Settings.setSettingForKey('user-id', undefined);
};

const getAccessToken = () => {
  return Settings.settingForKey('access-token');
};

export default function (context) {
  /**
   * To "log out", uncomment this line
   */
  // resetPluginState();

  let win = new BrowserWindow({
    width: 800,
    height: 750,
    frame: true,
    title: 'GitLab',
    backgroundColor: '#ffffff',
  });

  win.webContents.loadURL(require('./export.html'));

  win.webContents.on('needKey', (key) => {
    console.log('message:needKey');

    if (!getAccessToken()) {
      console.log('no access token. Loading login screen...');
      win.webContents.loadURL(GITLAB_OAUTH_AUTH_URL);
      return;
    }

    if (!Settings.settingForKey('user-id')) {
      fetch(`${GITLAB_API_BASE_URL}/user`, {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + getAccessToken(),
        },
      })
        .then((response) => {
          return response.json();
        })
        .then((success) => {
          Settings.setSettingForKey('user-id', success.id);
          let name;
          const document = sketch.fromNative(context.document);
          let layerArray = new Array();
          document.selectedLayers.forEach((layer) => {
            name = layer.name;
            layerArray.push({
              image: sketch.export(layer, { formats: 'png', output: false }),
              name: layer.name,
            });
          });
          win.webContents
            .executeJavaScript(`getImage(${JSON.stringify(layerArray)})`)
            .then((res) => {});
          win.webContents.executeJavaScript(
            'getKey("' +
              getAccessToken() +
              '", "' +
              name +
              '", "' +
              context.document.fileURL().lastPathComponent() +
              '", ' +
              Settings.settingForKey('user-id') +
              ')',
          );
        });
    } else {
      let name;
      const document = sketch.fromNative(context.document);
      let layerArray = new Array();
      document.selectedLayers.forEach((layer) => {
        name = layer.name;
        layerArray.push({
          image: sketch.export(layer, { formats: 'png', output: false }),
          name: layer.name,
        });
      });
      win.webContents
        .executeJavaScript(`getImage(${JSON.stringify(layerArray)})`)
        .then((res) => {});
      win.webContents.executeJavaScript(
        'getKey("' +
          getAccessToken() +
          '", "' +
          name +
          '", "' +
          context.document.fileURL().lastPathComponent() +
          '", ' +
          Settings.settingForKey('user-id') +
          ')',
      );
    }
  });

  win.webContents.on('newKey', (key) => {
    win.webContents.loadURL(GITLAB_OAUTH_AUTH_URL);
  });

  win.webContents.on('will-navigate', (event, url) => {
    if (url.startsWith('file://')) {
      win.show();
      return;
    }

    let parser = new URL(url);

    if (!parser) return;
    if (parser.host !== GITLAB_INSTANCE_HOST) {
      console.warn('Hostname incorrect');
      return;
    }
    if (!parser.hash.startsWith('#access')) {
      console.warn('No access code provided in redirect URL');
      return;
    }

    Settings.setSettingForKey(
      'access-token',
      parser.hash
        .replace('#access_token=', '')
        .toLowerCase()
        .replace('&token_type=bearer&state=test', ''),
    );

    fetch(`${GITLAB_API_BASE_URL}/user?access_token=${getAccessToken()}`, {})
      .then((res) => res.json())
      .then((success) => {
        Settings.setSettingForKey('user-id', success.id);
        win.destroy();
        win = new BrowserWindow({
          width: 800,
          height: 750,
          frame: true,
          title: 'GitLab',
          backgroundColor: '#ffffff',
        });
        win.loadURL(require('./export.html'), {
          extraHeaders: 'pragma: no-cache\n',
        });
        let name;
        const document = sketch.fromNative(context.document);
        let layerArray = new Array();
        document.selectedLayers.forEach((layer) => {
          name = layer.name;
          layerArray.push({
            image: sketch.export(layer, {
              formats: 'png',
              output: false,
            }),
            name: layer.name,
          });
        });
        win.webContents
          .executeJavaScript(`getImage(${JSON.stringify(layerArray)})`)
          .then((res) => {});
        win.webContents.executeJavaScript(
          'getKey("' +
            getAccessToken() +
            '", "' +
            name +
            '", "' +
            context.document.fileURL().lastPathComponent() +
            '", ' +
            Settings.settingForKey('user-id') +
            ')',
        );
      });
  });

  win.webContents.on('openTodo', (url) => {
    NSWorkspace.sharedWorkspace().openURL(NSURL.URLWithString(url));
  });

  win.webContents.on('submitComment', (projectId, issueId, markdown) => {
    exportToComment(projectId, issueId, markdown);
  });

  win.webContents.on('submitDesign', (projectPath, issueId) => {
    console.log('message:submitDesign');
    exportToDesigns(projectPath, issueId);
  });

  function exportToComment(projectId, issueId, markdown) {
    const document = sketch.fromNative(context.document);
    document.selectedLayers.forEach((layer) => {
      let file = sketch.export(layer, { formats: 'png', output: false });
      var form = new FormData();
      form.append('file', {
        fileName: layer.name + '.png',
        mimeType: 'image/png',
        data: file,
      });

      return fetch(`${GITLAB_API_BASE_URL}/projects/` + projectId + '/uploads', {
        method: 'POST',
        headers: {
          Authorization: 'Bearer ' + getAccessToken(),
          'Content-Type': 'multipart/form-data',
        },
        body: form,
      })
        .then((response) => {
          return response.json();
        })
        .then((success) => {
          return fetch(
            `${GITLAB_API_BASE_URL}/projects/${projectId}/issues/${issueId}/notes?body=${encodeURI(
              markdown.replace(
                '[' + layer.name + '](/' + layer.name + '.png)',
                success.markdown.replace('![', '['),
              ),
            )}`,
            {
              method: 'POST',
              headers: {
                Authorization: 'Bearer ' + getAccessToken(),
              },
            },
          )
            .then((response) => {
              return response.json();
            })
            .then((success) => {
              win.destroy();
              sketch.UI.message('Comment submitted 🚀');
            });
        });
    });
  }

  function exportToDesigns(projectPath, issueId) {
    const document = sketch.fromNative(context.document);
    let v = new Array();
    let m = {};
    let fd = new FormData();
    document.selectedLayers.forEach((layer, index) => {
      m[index] = ['variables.somefile.' + index];
      v.push(null);
      let file = sketch.export(layer, { formats: 'png', output: false });
      fd.append(index, {
        fileName: layer.name + '.png',
        mimeType: 'image/png',
        data: file,
      });
    });
    fd.append('map', JSON.stringify(m));
    let o = {
      query: `mutation uploadDesign($somefile: [Upload!]!) {
      designManagementUpload(input: {projectPath: "${projectPath}", iid: ${issueId}, files: $somefile}) {
        designs {
          id
          filename
        }
      }
    }`,
      variables: {
        somefile: v,
      },
    };
    fd.append('operations', JSON.stringify(o));

    return fetch(GITLAB_GRAPHQL_BASE_URL, {
      headers: {
        Accept: 'application/json',
        Authorization: 'Bearer ' + getAccessToken(),
        'Content-type': 'multipart/form-data',
      },
      body: fd,
      method: 'POST',
    })
      .then((result) => {
        return result.json();
      })
      .then((result) => {
        win.destroy();
        if (document.selectedLayers.length > 1) {
          sketch.UI.message('Designs uploaded 🚀');
        } else {
          sketch.UI.message('Design uploaded 🚀');
        }
      });
  }
}
